from calculator import Calculator
import yaml
import pytest
import allure

@pytest.fixture()
def calculate():
    print("开始计算")
    cal = Calculator()
    yield cal
    print("结束计算")

def get_datas():
    with open("./cal.yaml") as f:
        datas = yaml.safe_load(f)
        return datas
@allure.feature("测试计算器")
class TestCal:
    #参数化

    @pytest.mark.parametrize('a,b,expect',get_datas()['add_int']['datas'],ids=get_datas()['add_int']['ids'])
    @allure.story("int加法")
    @allure.severity(allure.severity_level.NORMAL)
    def test_add_int(self,calculate,a,b,expect):
        assert expect == calculate.add(a,b)

    @allure.severity(allure.severity_level.NORMAL)
    @pytest.mark.parametrize('a,b,expect',get_datas()['add_float']['datas'],ids=get_datas()['add_float']['ids'])
    @allure.story("float加法")
    def test_add_float(self,calculate, a, b, expect):
        assert expect == calculate.add(a,b)

    @allure.severity(allure.severity_level.NORMAL)
    @pytest.mark.parametrize('a,b,expect',get_datas()['sub']['datas'],ids=get_datas()['sub']['ids'])
    @allure.story("减法")
    def test_sub(self,calculate,a,b,expect):
        assert expect == calculate.add(a,b)

    @allure.severity(allure.severity_level.NORMAL)
    @pytest.mark.parametrize('a,b,expect',get_datas()['mul']['datas'],ids=get_datas()['mul']['ids'])
    @allure.story("乘法")
    def test_mul(self,calculate,a,b,expect):
        #round保留两位小数
        assert expect == round(calculate.mul(a,b),2)

    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize('a,b,expect',get_datas()['div']['datas'],ids=get_datas()['div']['ids'])
    @allure.story("除法")
    def test_div(self,calculate,a,b,expect):
        #assert expect == calculate.div(a,b)
        if expect == calculate.div(a,b):
            print("计算正确呀")
        elif b == 0:
            print("除数不能为0")




